angular.module('TidbitService', []).factory('Tidbit', ['$http', function($http) {
  return {
    // call to get all tidbits
    get: function() {
      return $http.get('/api/til');
    },


    // these will work when more API routes are defined on the Node side of things
    // app/routes.js
    // call to POST and create a new tidbit
    create: function(tidbitData) {
      return $http.post('/api/til', tidbitData);
    },

    // call to DELETE a tidbit
    delete: function(id) {
      return $http.delete('/api/til/' + id);
    }
  }
}]);
