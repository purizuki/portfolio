// Our Angular frontend will use the template file and
// inject it into the <div ng-view></div> in our index.html file.

angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', 
function($routeProvider, $locationProvider) {
  $routeProvider
    // home page
    .when('/', {
      templateUrl: 'views/home.html',
      controller: 'MainController'
    })
    // nerds page that will use the TidbitController
    .when('/til', {
      templateUrl: 'views/til.html',
      controller: 'TidbitController'
    });

  $locationProvider.html5Mode(true);
}]);
