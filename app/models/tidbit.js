// grab the mongoose module
var mongoose = require('mongoose');

// define our tidbit model
// module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model('Tidbit', {
  date: {type: Date, default: Date.now},
  body: String                     
}, 'til');
