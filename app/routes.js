// API routes are handled here. The rest go to Angular.

// grab the tidbit model we just created
var Tidbit = require('./models/tidbit');

module.exports = function(app) {
  // server routes ===========================================================
  // handle things like api calls
  // authentication routes

  app.get('/api/til', function(req, res) {
    // use mongoose to get all tidbits in the database
    Tidbit.find(function(err, tidbits) {
      // if there is an error retrieving, send the error.
      // nothing after res.send(err) will execute
      if(err) res.send(err);

      //console.log(tidbits);
      res.json(tidbits); // return all tidbits in JSON format
    });
  });
  app.post('/api/til', function(req, res) {
    var body = req.body.body.toString();
    console.log(body);
    
    Tidbit.insert({'body': body});
    res.end('yes');
  });
  
  app.get('/api', function(req, res) {
    res.send('nothing here');
  });
  app.get('/api/*', function(req, res) {
    res.send('nothing here');
  });

  // route to handle creating goes here (app.post)
  // route to handle delete goes here (app.delete)

  // frontend routes =========================================================
  // route to handle all angular requests
  app.get('*', function(req, res) {
    //console.log(__dirname);
    res.sendFile('../public/index.html', { root: __dirname }); // load our public/index.html file
    //res.sendfile('./public/index.html');
  });
};
